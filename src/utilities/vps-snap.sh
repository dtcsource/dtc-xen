# Check that the lv they requested actually exists!  
# Check if the VG stated is also correct!

##todo finish parameters, sort out email properly, do some logging, make a disk clean up routine
# report disks in system with sn if the sn check fails
# feature to just give us the sn's in the machine so we can figure out where to put the backups.
# for DTC intergration, likely also need to  know which disks are in the RAID and are USB so that we can't write to a local disk in error?
# Check we have enough space for the snapshot and the temp space on the VG.
# Prob should check if we have pv installed on the system.
# Add a folder for 'customer' to the parameters.

#!/bin/bash
#Author:  Don Gould - don@yournet.co.nz
#History:  
#June 2016 - Added backup to network features
# mount.cifs //IP/SHARE /mnt/bkserver --verbose -o "username=USERNAME"
#7 Dec 2015 - initial creation.
#Purpose - This script takes a snapshot of an LVM, compresses it, copies it to a USB disk, and then cleans up.

#Parameters - Volume Group name, Logical Volume to back up, Size of LV for snapshot, location of USB to copy the file to.
	#USBDiskSN:  This tells the program which disk to mount and put the backup on.
#Get the parameters to snap shot

#MailTo="tms@bowenvale.co.nz"
#MailAs="-aFrom:tms@bowenvale.co.nz"

#Catch the start time:
#start=`date +%s`
start=`date`

#Get the passed in parameters
for i in $@ ; do
	case "${1}" in
	"-VG")
		VG="${2}"
		shift
		shift
		;;
	"-LV")
		LV="${2}"
		shift
		shift
		;;
        "-USBDiskSN")
                USBDiskSN="${2}"
                shift
                shift
                ;;

        "-LVSnapSize")
                LVSnapSize="${2}"
                shift
                shift
                ;;

        "-MailTo")
                MailTo="${2}"
                shift
                shift
                ;;

        "-MailAs")
                MailAs="-aFrom:${2}"
                shift
                shift
                ;;

        "-ClientEmail")
                ClientEmail="${2}"
                shift
                shift
                ;;

	"-DestinationMode")
		# Options:  Disk / Network / DiskAndNetwork
		DestinationMode="${2}"
                shift
                shift
                ;;

	esac
done

# Execution prototype ./vps-snap.sh -VG xen-03vg -LV xen01 -USBDiskSN 444267D93080 -MailTo don@bowenvale.co.nz -MailAs node03@bowenvale.co.nz -LVSnapSize 1000MB -DestinationMode Network

Usage="A parameter was missing, usage should look like this: ./vps-snap.sh -VG xen-03vg -LV xen01 -USBDiskSN 444267D93080 -MailTo don@bowenvale.co.nz -MailAs node03@bowenvale.co.nz -LVSnapSize 1000MB"

# Report missing parameters
if [ -z $VG ]; then
	echo "-VG not set.  VG is the 'LVM Volume Group' that contains the LV (Logical Volume) that you want to back up."
	echo "$Usage"
	exit 0
fi

if [ -z $LV ]; then
        echo "-LV not set.  LV is the 'LVM Logical Volume' that you want to back up."
	echo "$Usage"
        exit 0
fi

if [ -z $DestinationMode ]; then
        echo "-DestinationMode not set.  This tells us if we're writting to a USB disk or Network share "
        echo "$Usage"
        exit 0
fi

if [ "$DestinationMode" = "Disk" -o "$DestinationMode" = "DiskAndNetwork" ];then
	if [ -z $USBDiskSN ]; then
        	echo "-USBDiskSN not set.  This is the Serial number (as reported by smartctl -i /dev/sd?1) of the USB disk that you're trying to write the back up to."
		echo "$Usage"
	        exit 0
	fi
fi

if [ -z $LVSnapSize ]; then
        echo "-LVSnapSize not set.  This is the amount of space that you want to allocate to the tempoary snap shot that will be created. You need to set this value in MB."
	echo "$Usage"
        exit 0
fi
if [ -z $MailTo ]; then
        echo "-MailTo not set.  This is the email address that will be used for status reports like errors and results."
	echo "$Usage"
        exit 0
fi
if [ -z $MailAs ]; then
        echo "-MailAs not set.  This is the email address that will be used in the 'from' address sent to the 'MailTo' address.  Set this so that replies to your status emails go to a logical place, like your helpdesk and not back to your VPS servers root address."
	echo "$Usage"
        exit 0
fi


# Now report what all the prameters are so that we can check we picked them up correctly.
echo "The backup is about to be performed with the following parameters"
echo
echo "VG: $VG"
echo "LV: $LV"
echo "USBDiskSN: $USBDiskSN"
echo "LVSnapSize: $LVSnapSize"
echo "MailTo: $MailTo"
echo "MailAs: $MailAs"

#Testing parameters
#exit 0
#VG="xen-03vg"
#LV="xen01"
#LVSnapSize="1000MB"
#LVSnapSize="1000000MB"
#USBDiskSN="444267D93080"
#USBDiskSN="444267D9eed0"


LVFull="/dev/$VG/$LV"
ThisHost=`hostname`
SnapDate=`date +%F-%H-%M`
LVSnapName="$LV-$SnapDate"
echo "LVSnapName:  $LVSnapName"

LVTempSpaceName="$LVSnapName-Temp"

#If we are using a disk we check if the disk is plugged in and if so, where it is plugged in...

if [ "$DestinationMode" = "Disk" -o "$DestinationMode" = "DiskAndNetwork" ]
	then
		#Return the sdX for the serial number given it.
		for i in /dev/sd?1
		 do
		   foo=`smartctl -i $i | grep Serial | awk '{print $3}' `
		        if [ "$foo" = "$USBDiskSN" ]
		        then
				USBDisk="$i"
		        fi
 		 done
		echo "USBDisk sd?1 value:  $USBDisk"


		#Test if we found the disk, if we didn't then we send an email and abandon
		if [ -z $USBDisk ]
		then
			#We didn't find the disk!
			(
			echo -e "Backup failed because we didn't find the disk with Serial Number: $USBDiskSN"
			)| mail -s "VPS Backup !!! FAILED - Disk NOT found - SN: $USBDiskSN !!! - $ThisHost - $LVFull" $MailTo $MailAs
			exit 0
		fi

		## THIS IS WHERE WE WANT TO CHECK IF WE JUST WANT TO DO A NETWORK BACKUP ONLY...
		# ADD A NEW SWITCH FOR NETWORK BACKUP, PERHAPS A LOCATION NOW?


		#Get details of the USB device so we can include it in the email.
		BackupVendor=`smartctl -i $USBDisk | grep Vendor | awk '{print $2}'`
		BackupProduct=`smartctl -i $USBDisk | grep Product | awk '{print $2}'`
		BackupSerial=` smartctl -i $USBDisk | grep Serial | awk '{print $3}'`
		BackupDevice="Disk:  $BackupVendor - $BackupProduct - Serial Number:  $BackupSerial"

fi
#Get the host name so we can include it in the email report.
ThisHost=`hostname`
if [ "$DestinationMode" = "Network" -o "$DestinationMode" = "DiskAndNetwork" ]
then
	#we'll record that the backup went to the network too
	BackupDevice="$BackupDevice - Network"
fi

echo "LVTempSpaceName:  $LVTempSpaceName"

FreeLVMSpace=`vgdisplay $VG --units M | grep "Free  PE / Size" | awk '{print $7 $8}'`
echo "FreeLVMSpace:  $FreeLVMSpace"
FreeLVMSpaceBeforeBackUp=$FreeLVMSpace

#Check if we have enough space to do the snapshot as requested.
#Take the "MB" off the values so we can perform some caculations on them.
nFreeLVMSpace=`echo -e $FreeLVMSpace | sed 's/MB//'`
nLVSnapSize=`echo -e $LVSnapSize | sed 's/MB//'`
nFreeLVMSpace=${nFreeLVMSpace%.*}
#nLVSnapSize=`echo -e ${$nLVSnapSize%.*}`
nLVSnapSize=${nLVSnapSize%.*}

echo "nFreeLVMSpace:  $nFreeLVMSpace"
echo "nLVSnapSize:  $nLVSnapSize"
if [ $nFreeLVMSpace -lt $nLVSnapSize ]
	then
		echo "FreeLVMSpace:  $FreeLVMSpace"
		echo "LVSnapSize:  $LVSnapSize"
		echo "Not enough VG disk space!"
		(
                echo -e "Backup failed because we don't have enough free disk space."
		echo "Free Volume Group Space:  $FreeLVMSpace"
                echo "Space Requested for Snapshot:  $LVSnapSize"
                )| mail -s "VPS Backup !!! FAILED - NOT Enough disk to Snapshot !!! - $ThisHost - $LVFull" $MailTo $MailAs
                exit 0
fi

SizeOfLVMtoSnap=`lvdisplay $LVFull --units M | grep "LV Size" | awk '{print $3 $4}'`
echo "SizeOfLVMToSnap:  $SizeOfLVMtoSnap"

#Check if we have enough VG space to make the tempoary area to do the img.gz file to.
  #Note - we are making the gz file to the local disk system because we're assuming its faster than attempting to write to the USB directly.
nSizeOfLVMtoSnap=`echo -e $SizeOfLVMtoSnap | sed 's/MB//'`
nSizeOfLVMtoSnap=${nSizeOfLVMtoSnap%.*}

#test
#nSizeOfLVMtoSnap=$((nSizeOfLVMtoSnap + 1000000))
echo "Checking that we have enough free space on the VG to do the backup"
echo "nFreeLVMSpace:  $nFreeLVMSpace"
echo "nSizeOfLVMToSnap: $nSizeOfLVMtoSnap"
if [ $nFreeLVMSpace -lt $nSizeOfLVMtoSnap ]
        then
                echo "FreeLVMSpace:  $FreeLVMSpace"
                echo "SizeOfLVMToSnap:  $SizeOfLVMtoSnap"
                echo "Not enough VG disk space for tempary area!"
              (
                echo -e "Backup failed because we don't have enough free disk space."
                echo "Free Volume Group Space:  $FreeLVMSpace"
                echo "Space Required for temp LV:  $SizeOfLVMtoSnap"
                )| mail -s "VPS Backup !!! FAILED - NOT Enough disk for Temp LV !!! - $ThisHost - $LVFull" $MailTo $MailAs
                exit 0
fi


if [ "$DestinationMode" = "Disk" -o "$DestinationMode" = "DiskAndNetwork" ]
then
	#Do we have enough free space on the USB disk?
	  echo "Mount the USB disk to store the img.gz file on:  /mnt/$LVSnapName-USB"
	  mkdir /mnt/$LVSnapName-USB
	  mount $USBDisk /mnt/$LVSnapName-USB

	  echo "Checking we have enough free disk space on the USB disk."
	  FreeUSB=`df | grep "/mnt/$LVSnapName-USB" | awk '{print $4}'`
	  FreeUSBh=`df -BM | grep "/mnt/$LVSnapName-USB" | awk '{print $4}'`
	  umount /mnt/$LVSnapName-USB
	  rm -rf /mnt/$LVSnapName-USB
	  echo "Free space on $USBDisk (bytes) - FreeUSB: $FreeUSB"
	  nFreeUSB=$((FreeUSB/1024))  # Convert the bytes back to MB so we can compare with required data sizes (bellow)
	  #test
	  #nFreeUSB=$((FreeUSB/1024000))

	  echo "nFreeUSB (MB):  $nFreeUSB"
	  if [ $nFreeUSB -lt $nSizeOfLVMtoSnap ]
	        then
        	        echo "nFreeUSB:  $nFreeUSB"
	                echo "SizeOfLVMtoSnap:  $SizeOfLVMtoSnap"
	                echo "Not enough USB disk space to Store Image!"
	              (
	                echo -e "Backup failed because we don't have enough free disk space on the USB device."
	                echo "Free USB Space:  $nFreeUSB"
	                echo "Space Required for Image:  $SizeOfLVMtoSnap"
			echo "Destination:              $BackupDevice"
			echo "The assumption is made that the gziping might not compress the LV at all, so we've allowed for the full size of the LV."
	                )| mail -s "VPS Backup !!! FAILED - NOT Enough USB disk space for LV image !!! - $ThisHost - $LVFull" $MailTo $MailAs
	                exit 0
	fi
fi

#Add the size of the LV to snap, to the name image name.  This is done to help you know how big to make your LV when you attempt to do a restore.
LVSnapName="$LVSnapName-$SizeOfLVMtoSnap"
echo "LVSnapName (with LV size included):  $LVSnapName"

#Code to restore and mount the snapshot
#Problems - need to know what size to restore to, this is hard to see when img.gz.  Maybe put size in name?
#lvcreate --size $SizeOfLVMtoSnap --name xenrestore $VG
#gzip -dc /mnt/usb1/VPSSnap/xen01snap-2015-12-07-20-17.img.gz | dd of=/dev/$VG/xenrestore bs=64k
#mkdir -p /mnt/xenrestore
#mount /dev/$VG/xenrestore /mnt/xenrestore


MailBody=`echo -e "$LVSnapName has been imaged and written to $BackupDevice as $LVSnapName.img.gz\n\n"`

#Testing stuff.
#FreeDisk=`df -h`
#MailBody="$MailBody $FreeDisk"
#exit 0

#(
#echo "Snapshot Name:		$LVSnapName"
#echo "Destination:		$BackupDevice"
#echo "Image Name:		$LVSnapName.img.gz"
#echo "Orginal LV Size:	$SizeOfLVMtoSnap" 
#echo
#echo "Disk usage:"
#df -h
#echo
#echo "iNode usage:"
#/bin/df -hi
#echo
#) | mail -s "VPS Backup - $ThisHost - $LVFull" $MailTo $MailAs
#echo $MailBody

#exit 0

#Checks
 #do we have enough free space on the USB?
 #do we have enough free space on the VG so that we can make the tempoary image?
#Reports
 #send email of actions taken, results

########################################################################
####  Now we do the actual backup, having completed all the checks! ####
########################################################################

#Make the Snapshot
echo "Making the snap shot:  lvcreate --size $LVSnapSize --snapshot --name $LVSnapName $LVFull"
lvcreate --size $LVSnapSize --snapshot --name $LVSnapName $LVFull

#Make a space to put the back up of the snapshot
  #Make the space at least as big as the orginal vm
	echo "Making a tempary space in the VG to compress the image to before we copy it to the USB disk:   lvcreate --size $SizeOfLVMtoSnap --name $LVTempSpaceName $VG"
	lvcreate --size $SizeOfLVMtoSnap --name $LVTempSpaceName $VG
	#Format the new space
	echo "Formatting the tempary space:   mkfs.ext3 /dev/$VG/$LVTempSpaceName"
	mkfs.ext3 /dev/$VG/$LVTempSpaceName

	#Mount the temp space
	echo "Mounting the tempary space:   mkdir /mnt/$LVTempSpaceName,  mount /dev/$VG/$LVTempSpaceName /mnt/$LVTempSpaceName"
	mkdir /mnt/$LVTempSpaceName
	mount /dev/$VG/$LVTempSpaceName /mnt/$LVTempSpaceName

	#Make the back up
	#echo "Compressing the snapshot to the tempary space:   dd if=/dev/$VG/$LVSnapName bs=64k | pv | gzip -c>/mnt/$LVTempSpaceName/$LVSnapName.img.gz"
	dd if=/dev/$VG/$LVSnapName bs=64k | pv | gzip -c>/mnt/$LVTempSpaceName/$LVSnapName.img.gz

	#Move the snapshot to my USB disk

	if [ "$DestinationMode" = "Disk" -o "$DestinationMode" = "DiskAndNetwork" ]
	then

		#Mount the USB
		echo "Mounting the USB disk:  mkdir /mnt/$LVSnapName-USB,  mount $USBDisk /mnt/$LVSnapName-USB"
		mkdir /mnt/$LVSnapName-USB
		mount $USBDisk /mnt/$LVSnapName-USB

		#Check I've got enough space to put the snapshot on the USB

		  #Copy the image to the USB
		    #Make a place to put it on the USB
		    mkdir -p /mnt/$LVSnapName-USB/VPSSnap

		    #Copy the file to the USB
			echo "Copying backup to USB disk:  cp /mnt/$LVTempSpaceName/$LVSnapName.img.gz /mnt/$LVSnapName-USB/VPSSnap/$LVSnapName.img.gz"
			cp /mnt/$LVTempSpaceName/$LVSnapName.img.gz /mnt/$LVSnapName-USB/VPSSnap/$LVSnapName.img.gz

			#Get the free disk space left on the USB disk, before we unmount it, so we can report that in the email.
			# FreeUSB <-- free space before the backup.
			FreeUSBAfterBackUp=`df | grep "/mnt/$LVSnapName-USB" | awk '{print $4}'`
			FreeUSBAfterBackUph=`df -BM | grep "/mnt/$LVSnapName-USB" | awk '{print $4}'`
			#Get the size of the backup
			BackupSize=`ls -lh /mnt/$LVSnapName-USB/VPSSnap/$LVSnapName.img.gz | awk '{print $5}'`
			BackupDetail=`ls -lh /mnt/$LVSnapName-USB/VPSSnap/$LVSnapName.img.gz`

	fi

	if [ "$DestinationMode" = "Network" -o "$DestinationMode" = "DiskAndNetwork" ]
	then

		#Copy the file to the network file share
		mkdir -p /mnt/bkserver/VPSSnap/$ThisHost/$VG/$LV/
		#Delete oldest files if more than 7...  <-- later we'll change this value to hold as many backups as the customer is paying for.
		#http://stackoverflow.com/questions/2960022/shell-script-to-count-files-then-remove-oldest-files
		ls -t /mnt/bkserver/VPSSnap/$ThisHost/$VG/$LV | sed -e '1,7d' | xargs -d '\n' rm
		cp /mnt/$LVTempSpaceName/$LVSnapName.img.gz /mnt/bkserver/VPSSnap/$ThisHost/$VG/$LV/$LVSnapName.img.gz
	fi

#Clean Up
	if [ "$DestinationMode" = "Disk" -o "$DestinationMode" = "DiskAndNetwork" ]
	then

		#Unmount the usb
		echo "Unmounting the USB Disk: umount /mnt/$LVSnapName-USB, rm -rf /mnt/$LVSnapName-USB"
		umount /mnt/$LVSnapName-USB
		rm -rf /mnt/$LVSnapName-USB
	fi

	#Delete the image file
	echo "Unmounting and removing the temp LV:  umount /mnt/$LVTempSpaceName, rm -rf /mnt/$LVTempSpaceName, lvremove --force /dev/$VG/$LVTempSpaceName"
	umount /mnt/$LVTempSpaceName
	rm -rf /mnt/$LVTempSpaceName
	lvremove --force /dev/$VG/$LVTempSpaceName

	#Remove the snapshot
	echo "Removing the Snapshot of the LV: lvremove --force /dev/$VG/$LVSnapName"
	lvremove --force /dev/$VG/$LVSnapName

	#Get VG free space
	FreeLVMSpace=`vgdisplay $VG --units M | grep "Free  PE / Size" | awk '{print $7 $8}'`
	echo "Free Space left on the VG after the backup is complted:  $FreeLVMSpace"
	echo "This is to help you identify if the disk didn't get cleaned up properly."

#We also need to include an admin email address so we can tell the VPS owner about the state of his system?
#We want to tell the customer about their backup and we want to tell our admin about the state of his resources?
#"Your back up has been completed, it used xxx mb on the back up device and you've been charged accordingly..."

#Post Process - email the user and admin.

#catch the end time
#end=`date +%s`
end=`date`
#runtime=$((end-start))

(
echo "Snapshot Name:            		$LVSnapName"
echo "Destination:              		$BackupDevice"
echo "Image Name:               		$LVSnapName.img.gz"
echo "Orginal LV Size:  			$SizeOfLVMtoSnap"
echo "Free disk space on USB before backup:	$FreeUSBh"
echo "Free disk space on USB after backup:	$FreeUSBAfterBackUph"
echo "Free VG space before backup:		$FreeLVMSpaceBeforeBackUp"
echo "Free VG space after backup:		$FreeLVMSpace"
echo "Backup Size:				$BackupSize"
echo "Backup Details:				$BackupDetail"
echo "Backup Execution Time:			$start to $end"
echo
echo "Disk usage:"
df -h
echo
echo "iNode usage:"
/bin/df -hi
echo
cat vps-snap-about.txt
) | mail -s "VPS Backup - $ThisHost - $LVFull" $MailTo $MailAs

if [ ! -z "$ClientEmail" ]; then
(
echo "Snapshot Name:                            $LVSnapName"
echo "Destination:                              $BackupDevice"
echo "Image Name:                               $LVSnapName.img.gz"
echo "Orginal LV Size:                          $SizeOfLVMtoSnap"
echo "Free disk space on USB before backup:     $FreeUSBh"
echo "Free disk space on USB after backup:      $FreeUSBAfterBackUph"
echo "Free VG space before backup:              $FreeLVMSpaceBeforeBackUp"
echo "Free VG space after backup:               $FreeLVMSpace"
echo "Backup Size:                              $BackupSize"
echo "Backup Details:                           $BackupDetail"
echo "Backup Execution Time:                    $start to $end"
echo
) | mail -s "VPS Backup - $ThisHost - $LVFull" $ClientEmail $MailAs

fi

echo
echo "Backup Completed - check $MailTo for results email."


