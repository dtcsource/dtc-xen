ABOUT VPS-SNAP.SH

This information was produced by vps-snap.sh.

This program creates a snapshot of a 'Logical Volume' on your LVM disk system, then compresses that snapshot to a img.gz file, then copies the resulting file to an external USB disk (with the serial number that you 

provided).

Checks

We make the following checks before execution.

* Did you provide all the required parameters?

* Is there enough free disk space in the VG to perform your request?
  We assume that you may need as much as 'LVSnapSize' + 'SizeOfLVToBeBackedUp' free in the VG to do your back up.
  We've assumed that you might not be able to get much compression advantage from gzip. 

* Is there enough free disk space on the USB disk to perform your request?
  We assume that you might need as much free USB disk space as the SizeOfLVToBeBackedUp free.

* Is a USB disk, with the serial number you provided, connected to the system?
  We as for the serial number so that we can figure out where the disk is plugged in that you want us to back up to.
  This value is also going to let us keep a record of where backups went for you.

To Do:

* Logging - we still want to make a log of where you saved backups to.  This will eventually be passed back to DTC so it can be viewed in the panel.
* SOAP intergration for DTC-Xen.  We want to request backups from the panel.

* Timmer - we want to know how long a back up took so that we can figure how what backups can be done.

* Schedualer - we want to be able to schedual backups to be done in case a bunch of backups are requrested.  I'm not sure how we're going to do that at present?

* DTC panel intergration to requrest backups.
  -  A way to download your backups that's metered.  From a charging pov I want to charge customers to do a backup (because it costs me CPU), charge to store the backup (because that costs me disks), a charge to pull 

the backup off out servers (because that costs me traffic) and to restore a backup (and I'm suspecting that last one will have to be 'on requrest' and we need a way for customers to 'mount' their backup so they can 

just drag data out of them).



