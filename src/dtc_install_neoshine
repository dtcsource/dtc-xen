#!/bin/sh

CACHEDIR="/var/cache/yum"      # where yum caches stuff -- is created as a subdir
                               # of the destination chroot

# FIXME perhaps after installation the script can modify the target machine's yum config to point to our Squid proxy
#       or define http_proxy inside the machine.  that would make upgrades for customers much much faster.
# better idea: instead of using a web cache, use a stash on the machine, we rsync the new RPMs into it once it's finished
# we would need a mutex (flock or fcntl based?) that mutially excludes the critical section
# the critical section is both the yum and the rsync process
# we also need to rsync packages from the stash into the var cache on the vps, and a mutex to lock out if another yum is running, just as in the first scenario
# cannot use a symlink because its chrooted for the duration of the process
# at any case, the repo names for different distros need to be different, otherwise the caches will clash horribly
# FIXME once that is done, we can stop using apt-proxy or apt-cacher
# FIXME try to make it for suse, mandriva or any other rpm-based distro

YUMENVIRON="$1"                # where the yum config is generated and deployed
INSTALLROOT="$2"               # destination directory / chroot for installation

if [ "${INSTALLROOT}" = "" -o ! -d "${INSTALLROOT}" -o "${YUMENVIRON}" = "" ] ; then
	echo "usage: centos-installer /yum/environment (will be created) /destination/directory (must exist)"
	echo "dest dir MUST BE an absolute path"
	exit 126
fi

set -e
set -x

which rpm >/dev/null 2>&1 || { echo "rpm is not installed.  please install rpm." ; exit 124 ; }

# sometimes when the RPM database is inconsistent, yum fails but exits with success status
# we make sure the db is in good health
mkdir -p /var/lib/rpm
rpm --rebuilddb

# set distro ver
releasever=5

# detect architecture
ARCH=`uname -m`
if [ "${ARCH}" = x86_64 ] ; then
	exclude="*.i386 *.i586 *.i686"
	basearch=x86_64
elif [ "${ARCH}" = i686 ] ; then
	exclude="*.x86_64"
	basearch=i386
else
	echo "Unknown architecture: ${ARCH} -- stopping centos-installer"
	exit 3
fi

# make yum environment

mkdir -p ${YUMENVIRON}/pluginconf.d ${YUMENVIRON}/repos.d ${CACHEDIR} ${INSTALLROOT}/var/log

# In case the folder is not there:
mkdir -p /var/lib/rpm

# configure yum:

cat > "${YUMENVIRON}/yum.conf" << EOF
[main]
reposdir=$YUMENVIRON/repos.d
pluginconfpath=$YUMENVIRON/pluginconf.d
cachedir=$CACHEDIR
installroot=$INSTALLROOT
exclude=$exclude
keepcache=1
#debuglevel=4
#errorlevel=4
pkgpolicy=newest
distroverpkg=centos-release
tolerant=1
exactarch=1
obsoletes=1
gpgcheck=1
plugins=1
metadata_expire=1800
EOF

cat > "${YUMENVIRON}/pluginconf.d/installonlyn.conf" << EOF
[main]
enabled=1
tokeep=5
EOF

cat > "${YUMENVIRON}/repos.d/ND6.repo" << EOF
[base]
name=nd6 - Base
baseurl=http://10.3.1.135/repo/ND/6/x86_64/os/
gpgcheck=0

[base2]
name=fc13 - Base
baseurl=http://10.3.1.135/repo/fedora/releases/13/Everything/x86_64/os/
gpgcheck=0
EOF

# unleash yum

export LANG=C
exec yum -c "${YUMENVIRON}/yum.conf" -y install basesystem neokylin-release yum wget which yum-basearchonly nano rsyslog passwd joe screen
