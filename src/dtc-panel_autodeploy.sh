#!/bin/sh
# Given the fact that your VPS is well configured
# with a correct hostname ans IP, this script will
# setup DTC in ONCE, by just giving a password that
# will be set for the root MySQL user, DTC and phpmyadmin.
#
# This will only run on Debian based VPS, it should
# run well with Squeeze, but it's not tested if using Ubuntu.

set -e

# Check number of params and print usage.
if ! [ $# = 1 ] ; then
	echo "Usage: dtc-autodeploy <password-to-set>"
	exit 1
fi

# Get the password to set using the command line...
PASSWORD=$1

export DEBCONF_FRONTEND=noninteractive
APTGET="apt-get -o Dpkg::Options::=--force-confnew --force-yes -fuy"

apt-get update
${APTGET} install ssh
${APTGET} dist-upgrade

# Set the apt to NOT install the recommends, to make it a smaller footprint
echo "APT{
Install-Recommends \"false\";
}" >/etc/apt/apt.conf

# Find the hostname and default interface and IP of the VPS
DOMAIN_NAME=`hostname --domain`
DEFAULT_IF=`/sbin/route | grep default |awk -- '{ print $8 }'`
IP_ADDR=`ifconfig ${DEFAULT_IF} | grep 'inet addr' | sed 's/.\+inet addr:\([0-9.]\+\).\+/\1/'`

# Set the values in debconf
MKTEMP="mktemp -t"

SETSEL_FILE=`${MKTEMP} DTC_AUTODEPLOY.XXXXXX` || exit 1

#DEBCONF_FRONTEND=noninteractive apt-get --force-yes --assume-yes install squirrelmail squirrelmail-locales
${APTGET} install squirrelmail squirrelmail-locales

# Copy our selection_config_file template file, and tweak it with correct values
cp selection_config_file ${SETSEL_FILE}

sed -i "s/__PASSWORD__/${PASSWORD}/g" ${SETSEL_FILE}
sed -i "s/__DOMAIN_NAME__/${DOMAIN_NAME}/g" ${SETSEL_FILE}
sed -i "s/__IP__ADDRESS__/${IP_ADDR}/g" ${SETSEL_FILE}

# Set the values needed to setup DTC
debconf-set-selections ${SETSEL_FILE}

${APTGET} install dtc-toaster

# Sets back debconf to interactive mode with priority medium,
# so that the user can install new packages (like roundcube)
# more comfortably.
echo "debconf debconf/frontend select Dialog
debconf debconf/frontend seen true
debconf debconf/priority select medium
debconf debconf/priority seen true
debconf debconf-apt-progress/title string fake
debconf debconf-apt-progress/title seen true
debconf debconf-apt-progress/preparing string fake
debconf debconf-apt-progress/preparing seen true" >${SETSEL_FILE}
debconf-set-selections ${SETSEL_FILE}
dpkg-reconfigure -f noninteractive debconf

# Finally start the dtc shell installer and we are done!
/usr/share/dtc/admin/install/install

sleep 2
invoke-rc.d apache2 restart
